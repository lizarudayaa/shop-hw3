import React, {useState} from 'react';
import FavoriteItems from "../../components/FavoriteItems/FavoriteItems";

const Favorites = () => {

    const [favItems , setFavItems] = useState( JSON.parse(localStorage.getItem('fav')) || [])

    return (
        <div>
         <FavoriteItems favBooks={favItems} setFavItems={(item) => setFavItems(item)} />
        </div>
    );
};

export default Favorites;