import React, {useEffect, useState} from 'react';
import ItemsList from "../../components/Items/ItemsList";
import axios from "axios";
import Loader from "../../UI/Loader/Loader";

const Home = () => {
    const[isLoading , setIsLoading] = useState(true)
    const [data, setData] = useState([])
    const [favorites , setFavorites] = useState( JSON.parse(localStorage.getItem('fav')) || [])


    useEffect(() => {
        axios('./items.json')
            .then(r => {
                setData(r.data)
                setIsLoading(false)
            })
    }, [])



    return (
        <>
            {isLoading && <Loader/>}
            {!isLoading && <ItemsList items={data} favorites={favorites} setFavorites={(item) =>setFavorites(item) } />}
        </>
    );
};

export default Home;