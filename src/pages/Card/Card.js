import React, {useState} from 'react';
import CardItems from "../../components/CardItems/CardItems";
import Modal from "../../components/Modal/Modal";
import {modals} from "../../components/Modal/ModalDeclaration";


const Card = () => {

    const [cardItems , setCardItem] = useState( JSON.parse(localStorage.getItem('cart')) || [])
    const [currCart, setCurrCart] = useState(null)
    const [modalId, setModalId] = useState(null)

    const modal = modals.find(item => item.modalID === modalId)

    const openModal = id =>{
        setModalId(id)
    }
    const closeModal = () =>{
        setModalId(null)
    }

    const deleteBook = (item) => {
        const books = cardItems.filter( i => i.id !== item.id)
        localStorage.setItem('cart', JSON.stringify(books))
        setCardItem(books)
    }
    return (
        <div>
            <CardItems
                currCard={(card) => setCurrCart(card)}
                openModal={data => {
                    setCurrCart(data)
                    openModal(2)
                }}
                cardItems={cardItems}
            />
            {modal && <Modal
                key={modal.key}
                header={modal.header}
                text={modal.text}
                closeModal={closeModal}
                actions={
                    modal.actions({
                        agree: () => {
                            deleteBook(currCart)
                            closeModal()
                        },
                        disagree: () => closeModal()
                    })
                }
            />}

        </div>
    );
};

export default Card;