
import './App.css';
import Footer from "./UI/Footer/Footer";
import Header from "./UI/Header/Header";
import AppRoutes from "./AppRoutes/AppRoutes";

function App() {

  return (
    <div className="app-wrapper">
        <div className="app">
            <Header/>
            <AppRoutes />

        </div>
        <Footer />
    </div>
  );
}

export default App;
