import React, {useState} from 'react';
import Button from "../../UI/Button/Button";
import Star from "../../UI/icons/Star";
import Modal from '../Modal/Modal'
import {modals} from "../Modal/ModalDeclaration";
import {Card, CardIconStar, CardImg, CardImgWrapper} from "../../UI/Styles/Styles";
import styles from './Items.module.scss';


const Item = ({setFavorites, data , isFavorite }) => {

    const [modalId , setModalId]= useState(null)


    const openModal = (id) => {
        setModalId(id)
    }
    const closeModal = () => setModalId(null)

    const modal = modals.find(i => i.modalID === modalId)

    const addToCart = (book) => {
        const cart = JSON.parse(localStorage.getItem('cart')) || []
        cart.push(book)
        localStorage.setItem('cart', JSON.stringify(cart));
    }
    const addToFavorites = (book) => {
        const favorites = JSON.parse(localStorage.getItem('fav')) || []
        const favoriteItems = favorites.findIndex(item => book.id === item.id)
        if (favoriteItems === -1){
            favorites.push(book)
        } else {
            favorites.splice(favoriteItems,1)
        }

        localStorage.setItem('fav', JSON.stringify(favorites))
        setFavorites(favorites)
    }


    return (<Card >
                    <CardImgWrapper >
                        <CardImg src={data.img} alt={data.name} />
                        <CardIconStar  onClick={() => addToFavorites(data)} >
                            {isFavorite? <Star color="yellow" filled={'yellow'}/> : <Star color="black"/> }
                        </CardIconStar>
                    </CardImgWrapper>
                        <div className={styles.header}>
                            <p className={styles.title}>{data.name}</p>
                        </div>
                        <div className={styles.body}>
                            <p className={styles.price}>{data.price}<span>$</span></p>
                            <Button color='black' text='add to card' onClick={() => openModal(1)} />
                        </div>

            <div>
                {modal && <Modal
                    closeModal={closeModal}
                    header={modal.header}
                    text={modal.text}
                    actions={modal.actions({
                        btnClose: () => {
                            closeModal()
                        },
                        btnAdd: () => {
                            addToCart(data)
                           closeModal()
                        }
                    })}
                />}
            </div>
            </Card>

    );
};

export default Item;