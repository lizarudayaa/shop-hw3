import React from 'react';
import Item from "./Item";
import {Wrapper} from "../../UI/Styles/Styles";

const ItemsList = ({items , favorites, setFavorites}) => {

    const isFavourite = item => favorites.findIndex(book => book.id === item.id) === -1 ? false : true;
    const books = items.map(book => <Item
        key={book.id}
        data={book}
        isFavorite={isFavourite(book)}
        setFavorites={setFavorites}
    />)


    return (
            <Wrapper>
                {books}
            </Wrapper>


    );
};

export default ItemsList;