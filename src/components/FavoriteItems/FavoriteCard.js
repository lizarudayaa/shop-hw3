import React from 'react';
import {Card, CardIconStar, CardImg, CardImgWrapper} from "../../UI/Styles/Styles";
import Star from "../../UI/icons/Star";

const FavoriteCard = ({book, onClick}) => {
    return (
        <div>
            <Card key={book.id} >
                    <CardImgWrapper >
                        <CardImg src={book.img}/>
                        <CardIconStar onClick={onClick} >
                            <Star color="yellow" filled={'yellow'}/>
                        </CardIconStar>
                    </CardImgWrapper>
            </Card>
        </div>
    );
};

export default FavoriteCard;