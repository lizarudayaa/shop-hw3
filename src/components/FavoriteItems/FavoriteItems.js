import React from 'react';
import {useHistory} from "react-router-dom";
import Button from "../../UI/Button/Button";
import { NoItems, Wrapper} from "../../UI/Styles/Styles";
import FavoriteCard from "./FavoriteCard";



const FavoriteItems = ({favBooks, setFavItems}) => {

    const history = useHistory();

    const removeFavorite = (book) => {
        const favorites = favBooks.filter(fav => book.id !== fav.id)
        setFavItems(favorites)
        localStorage.setItem('fav', JSON.stringify(favorites))
    }

    const favoriteList = favBooks.map(book => <FavoriteCard key={book.id} book={book} onClick={() =>removeFavorite(book)} />);


    if(!favBooks.length){
        return (
            <NoItems>
                <h3>No items found</h3>
                <Button color='black' text='Products' onClick={() => history.push('/home')}/>
            </NoItems>
        )
    }
    return (
        <Wrapper>
            {favoriteList}
        </Wrapper>
    );
};

export default FavoriteItems;