import React from 'react';
import Button from "../../UI/Button/Button";

const CardItem = ({item , onClick}) => {
    return (
        <div className='basket_product' key={item.id}>
            <div className="item">
                <div>
                    <img src={item.img} alt={item.name} className='basket_image'/>
                </div>
                <div className="basket_details">
                    <h2>{item.name}</h2>
                    <p>Product code #{item.id}</p>
                </div> </div>
            <div className="price">{item.price}</div>
            <div className="quantity">
                <p className='quantity-field'>1</p>
            </div>
            <div className="subtotal">{item.price}</div>
            <div className="remove">
                <Button color='black' text='Remove' onClick={onClick}/>
            </div>
        </div>
    );
};

export default CardItem;