import React from 'react';
import {useHistory} from "react-router-dom";
import './CardItems.scss'
import Button from "../../UI/Button/Button";
import Basket from "../../UI/Basket/Basket";
import Summary from "../../UI/Summary/Summary";
import {NoItems} from "../../UI/Styles/Styles";
import CardItem from "./CardItem";


const finalAmount = (arr) => {
    return arr.map(book => +(book.price)).reduce((accumulator, currentValue) => accumulator + currentValue, 0)
};



const CardItems = ({openModal , cardItems}) => {

    const history = useHistory();

    const cartList = cardItems.map(item => <CardItem key={item.id} item={item} onClick={() => openModal(item)}/>)


    if(!cardItems.length){
        return (
            <NoItems>
                <h3>No items in card</h3>
                <Button text={'Products'} color={'black'} onClick={() => history.push('/home')}/>
            </NoItems>
        )
    }
    return (
        <div className='main'>
            <div className="basket">
                <Basket/>
                <div>
                    {cartList}
                </div>
            </div>
            <Summary amount={finalAmount(cardItems)}/>


        </div>
    );
};

export default CardItems;