import React from 'react';
import styles from './Header.module.scss'
import {GiBookshelf } from 'react-icons/gi';
import Nav from "../../components/Navigation/Nav";
const Header = () => {
    return (
        <header>
            <div className={styles.header}>
                <div className={styles.logo}>
                    <span><GiBookshelf size='7em'/></span>
                </div>
                <div className={styles.title}>
                    <h3>Bookieyork</h3>
                </div>
            </div>
            <Nav/>


        </header>
    );
};

export default Header;