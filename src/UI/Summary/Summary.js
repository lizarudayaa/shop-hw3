import React from 'react';

const Summary = ({amount}) => {
    return (
        <div className="aside">
            <div className="summary">
                <div className="summary_total">
                    <span className="summary_total_text">
                    Checkout
                    </span>
                </div>
                <div className="summary_subtotal">
                    <div className="summary_subtotal_title">
                        Subtotal
                    </div>
                    <div className="summary_subtotal_value">
                        {amount}
                    </div>
                </div>
            </div>
        </div>
    )
};

export default Summary ;
